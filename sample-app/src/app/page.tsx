import Accordion from '@/components/elements/Accordion'
import Alert from '@/components/elements/Alert'
import Avatar from '@/components/elements/Avatar'
import Badge from '@/components/elements/Badge'
import Breadcrumb from '@/components/elements/Breadcrumb'
import Button from '@/components/elements/Button'
import ButtonGroup from '@/components/elements/ButtonGroup'
import Card from '@/components/elements/Card'
import Checkbox from '@/components/elements/Checkbox'
import DefalutFormLayout from '@/components/elements/DefalutFormLayout'
import Divider from '@/components/elements/Divider'
import Dropdown from '@/components/elements/Dropdown'
import FileInput from '@/components/elements/FileInput'
import Input from '@/components/elements/Input'
import List from '@/components/elements/List'
import MediaCard from '@/components/elements/MediaCard'
import Modal from '@/components/elements/Modal'
import Notification from '@/components/elements/Notification'
import Pagination from '@/components/elements/Pagination'
import RadioButton from '@/components/elements/RadioButton'
import SelectBox from '@/components/elements/SelectBox'
import Table from '@/components/elements/Table'
import Textarea from '@/components/elements/Textarea'
import Toggle from '@/components/elements/Toggle'
import Tooltip from '@/components/elements/Tooltip'
import Image from 'next/image'
import 'tailwindcss/tailwind.css'

export default function Home() {
  return (
    <>
    <div className='bg-slate-50'>
      <Accordion />
      <Alert />
      <Avatar />
      <Badge />
      <Breadcrumb />
      <Button />
      <ButtonGroup />
      <Card />
      <Divider />
      <List />
      <MediaCard />
      <Notification />
      <Pagination />
      <Table />
      <Checkbox />
      <FileInput />
      <DefalutFormLayout />
      <Input />
      <RadioButton />
      <SelectBox />
      <Textarea />
      <Toggle />
    </div>
    </>
  )
}
