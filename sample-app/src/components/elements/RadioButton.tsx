import React from 'react'

export default function RadioButton() {
  return (
    <>
    <div className="mx-auto space-y-3">
      <div className="flex items-center space-x-2">
        <input type="radio" id="example1" name="radioGroup1" className="h-4 w-4 rounded-full border-gray-300 text-primary-600 shadow-sm focus:border-primary-300 focus:ring focus:ring-primary-200 focus:ring-opacity-50 focus:ring-offset-0 disabled:cursor-not-allowed disabled:text-gray-400" />
        <label className="text-sm font-medium text-gray-700">Default</label>
      </div>
      <div className="flex items-center space-x-2">
        <input type="radio" id="example2" name="radioGroup1" className="h-4 w-4 rounded-full border-gray-300 text-primary-600 shadow-sm focus:border-primary-300 focus:ring focus:ring-primary-200 focus:ring-opacity-50 focus:ring-offset-0 disabled:cursor-not-allowed disabled:text-gray-400" />
        <label className="text-sm font-medium text-gray-700">Checked</label>
      </div>
    </div>
    </>
  )
}
