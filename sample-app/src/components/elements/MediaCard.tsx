import React from 'react'

export default function MediaCard() {
  return (
    <>
        <div className="mx-auto max-w-xl">
        <div className="flex gap-4">
            <img className="h-12 w-12 rounded object-cover object-center" src="https://images.unsplash.com/photo-1487309078313-fad80c3ec1e5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="" />
            <div className="flex-1">
            <h4 className="text-lg font-medium">Media title</h4>
            <p className="mt-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ac nisl quis massa vulputate adipiscing.</p>
            </div>
        </div>
        </div>
    </>
  )
}
