import React from 'react'

export default function Checkbox() {
  return (
    <>
    <div className="space-y-3">
      <div className="flex items-center space-x-2">
        <input type="checkbox" id="example1" className="h-4 w-4 rounded border-gray-300 text-primary-600 shadow-sm focus:border-primary-300 focus:ring focus:ring-primary-200 focus:ring-opacity-50 focus:ring-offset-0 disabled:cursor-not-allowed disabled:text-gray-400" />
        <label className="text-sm font-medium text-gray-700">Default</label>
      </div>
      <div className="flex items-center space-x-2">
        <input type="checkbox" id="example2" checked className="h-4 w-4 rounded border-gray-300 text-primary-600 shadow-sm focus:border-primary-300 focus:ring focus:ring-primary-200 focus:ring-opacity-50 focus:ring-offset-0 disabled:cursor-not-allowed disabled:text-gray-400" />
        <label className="text-sm font-medium text-gray-700">Checked</label>
      </div>
    </div>
    </>
  )
}
