import React from 'react'

export default function ButtonGroup() {
  return (
    <>
        <div className="space-x-5">
        <div className="inline-flex -space-x-0 divide-x divide-gray-300 overflow-hidden rounded-lg border border-gray-300 shadow-sm">
            <button type="button" className="bg-white px-4 py-2.5 text-center text-sm font-medium text-secondary-700 hover:bg-gray-100">Prev</button>
            <button type="button" className="bg-white px-4 py-2.5 text-center text-sm font-medium text-secondary-700 hover:bg-gray-100">Next</button>
        </div>
        <div className="inline-flex -space-x-0 divide-x divide-gray-300 overflow-hidden rounded-lg border border-gray-300 shadow-sm">
            <button type="button" className="bg-white px-4 py-2.5 text-center text-sm font-medium text-secondary-700 hover:bg-gray-100">One</button>
            <button type="button" className="bg-white px-4 py-2.5 text-center text-sm font-medium text-secondary-700 hover:bg-gray-100">Two</button>
            <button type="button" className="bg-white px-4 py-2.5 text-center text-sm font-medium text-secondary-700 hover:bg-gray-100">Three</button>
        </div>
        </div>
    </>
  )
}
