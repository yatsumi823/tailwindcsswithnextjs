import React from 'react'

export default function Textarea() {
  return (
    <>
    <div className="mx-auto max-w-xs">
      <div>
        <label className="mb-1 block text-sm font-medium text-gray-700">Message</label>
        <textarea id="example1" className="block w-full rounded-md border-gray-300 shadow-sm focus:border-primary-300 focus:ring focus:ring-primary-200 focus:ring-opacity-50 disabled:cursor-not-allowed disabled:bg-gray-50" placeholder="Leave a message"></textarea>
      </div>
    </div>
    </>
  )
}
