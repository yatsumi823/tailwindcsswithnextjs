import React from 'react'

export default function Input() {
  return (
    <>
    <div className="mx-auto max-w-xs">
      <input type="text" className="block w-full rounded-md border-gray-300 shadow-sm focus:border-primary-400 focus:ring focus:ring-primary-200 focus:ring-opacity-50 disabled:cursor-not-allowed disabled:bg-gray-50 disabled:text-gray-500" placeholder="Name" />
    </div>
    </>
  )
}
