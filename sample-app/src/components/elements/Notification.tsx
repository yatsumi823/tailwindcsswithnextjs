import React from 'react'

export default function Notification() {
  return (
    <>
    <div className="space-y-5">
      <div className="relative mx-auto max-w-[400px] rounded-xl border border-secondary-50 bg-white p-4 text-sm shadow-lg">
        <button className="absolute top-4 right-4 ml-auto text-secondary-500 hover:text-secondary-900">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" className="h-5 w-5">
            <path d="M6.28 5.22a.75.75 0 00-1.06 1.06L8.94 10l-3.72 3.72a.75.75 0 101.06 1.06L10 11.06l3.72 3.72a.75.75 0 101.06-1.06L11.06 10l3.72-3.72a.75.75 0 00-1.06-1.06L10 8.94 6.28 5.22z" />
          </svg>
        </button>
        <div className="flex space-x-4">
          <div className="flex-1">
            <h4 className="pr-6 font-medium text-secondary-900">Updates have been made to your profile</h4>
            <div className="mt-1 text-secondary-500">Your team has made changes to your company profile since you last logged in.</div>
            <div className="mt-2 flex space-x-4">
              <button className="inline-block font-medium leading-loose text-secondary-500 hover:text-secondary-900">Dismiss</button>
              <button className="inline-block font-medium leading-loose text-primary-600 hover:text-primary-700">View more</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    </>
  )
}
