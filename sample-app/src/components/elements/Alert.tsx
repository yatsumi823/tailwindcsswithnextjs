import React from 'react'

function Alert() {
  return (
    <>
        <div className="space-y-4">
            <div className="rounded-md bg-primary-50 p-4 text-sm text-primary-500"><b>Info alert</b> Lorem ipsum dolor sit amet. Internos reprehenderit perspiciatis commodi et omnis impedit.</div>
            <div className="rounded-md bg-green-50 p-4 text-sm text-green-500"><b>Success alert</b> Lorem ipsum dolor sit amet. Internos reprehenderit perspiciatis commodi et omnis impedit.</div>
            <div className="rounded-md bg-yellow-50 p-4 text-sm text-yellow-500"><b>Warning alert</b> Lorem ipsum dolor sit amet. Internos reprehenderit perspiciatis commodi et omnis impedit.</div>
            <div className="rounded-md bg-red-50 p-4 text-sm text-red-500"><b>Error alert</b> Lorem ipsum dolor sit amet. Internos reprehenderit perspiciatis commodi et omnis impedit.</div>
        </div>
    </>
  )
}

export default Alert