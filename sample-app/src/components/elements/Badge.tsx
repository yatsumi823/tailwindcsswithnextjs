import React from 'react'

export default function Badge() {
  return (
    <>
        <div className="flex flex-wrap justify-center gap-2">
        <span className="rounded-full bg-primary-50 px-2 py-1 text-xs font-semibold text-primary-600"> Badge </span>
        <span className="rounded-full bg-green-50 px-2 py-1 text-xs font-semibold text-green-600"> Badge </span>
        <span className="rounded-full bg-yellow-50 px-2 py-1 text-xs font-semibold text-yellow-600"> Badge </span>
        <span className="rounded-full bg-red-50 px-2 py-1 text-xs font-semibold text-red-600"> Badge </span>
        <span className="rounded-full bg-gray-100 px-2 py-1 text-xs font-semibold text-gray-600"> Badge </span>
        <span className="rounded-full bg-black px-2 py-1 text-xs font-semibold text-white"> Badge </span>
        </div>
    </>
  )
}
