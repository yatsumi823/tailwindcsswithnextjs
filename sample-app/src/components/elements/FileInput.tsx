import React from 'react'

export default function FileInput() {
  return (
    <>
    <div className="mx-auto max-w-xs">
      <label className="mb-1 block text-sm font-medium text-gray-700">Upload file</label>
      <input id="example2" type="file" className="block w-full text-sm file:mr-4 file:rounded-md file:border-0 file:bg-primary-500 file:py-2.5 file:px-4 file:text-sm file:font-semibold file:text-white hover:file:bg-primary-700 focus:outline-none disabled:pointer-events-none disabled:opacity-60" />
      <p className="mt-1 text-sm text-gray-500">This is a help message.</p>
    </div>
    </>
  )
}
