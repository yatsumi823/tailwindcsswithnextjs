import React from 'react'

export default function List() {
  return (
    <>
        <div className="mx-auto max-w-lg">
        <ul className="ml-4 list-disc">
            <li>Share team inboxes</li>
            <li>Deliver instant answers</li>
            <li>Manage your team with reports</li>
            <li>Connect with customers</li>
            <li>Connect the tools you already use</li>
        </ul>
        </div>
    </>
  )
}
