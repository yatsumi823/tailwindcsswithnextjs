import React from 'react'

export default function SelectBox() {
  return (
    <>
    <div className="mx-auto max-w-xs">
      <select id="example1" className="block w-full rounded-md border-gray-300 shadow-sm focus:border-primary-300 focus:ring focus:ring-primary-200 focus:ring-opacity-50 disabled:cursor-not-allowed disabled:bg-gray-50">
        <option value="">Option01</option>
        <option value="">Option02</option>
        <option value="">Option03</option>
      </select>
    </div>
    </>
  )
}
